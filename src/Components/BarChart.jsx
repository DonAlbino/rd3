import React, { Component } from 'react';
import * as d3 from 'd3';

class BarChart extends Component {

    componentDidMount(){
        this.drawChart();
    }

    drawChart(){
        const data = [100, 5, 6, 6, 9, 10];
        const w = 700;
        const h = 300;

        const svg = d3.select("body")
        .append("svg")
        .attr("width", w)
        .attr("height", h)
        .style("margin-left",100);
        
        svg.selectAll("rect")
        .data(data)
        .enter()
        .append("rect")
        .attr("x", (d, i) => i * (w/data.length))
        .attr("y", (d, i) => h - 2 * d)
        .attr("width", 65)
        .attr("height", (d, i) => d * 10)
        .attr("fill", "green");

        svg.selectAll("text")
        .data(data)
        .enter()
        .append("text")
        .text((d) => d)
        .attr("x", (d, i) => i * (w/data.length) + 20)
        .attr("y", (d, i) => h - (2 * d) - 3)

    }

    render(){
        return <div id={"#" + this.props.id}></div>
      }

}
 
export default BarChart;
