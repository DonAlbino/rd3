import React, { Component } from 'react';
import * as d3 from 'd3';
import data from '../data/SuperHeroInfo.json'
import { publicDecrypt } from 'crypto';

class RandataBarChart extends Component {

    render(){
        return <div id={"#" + this.props.id}></div>
      }
    
    componentDidMount(){

        var Publisher = d3.nest()
            .key( function (d) {return d.Publisher})
            .rollup(function (v) {return v.length})
            .entries(data);

        var Gender = d3.nest()
            .key( function (d) {return d.Gender})
            .rollup(function (v) {return v.length})
            .entries(data);
        
        console.log(Publisher);
        console.log(Gender);
        
        this.drawChart(Publisher);

    }
    
    componentWillReceiveProps(nextProps){
        this.drawChart(nextProps);
    }

    shouldComponentUpdate(){
        return false;
    }

    drawChart(data){
        const h = 600;
        const w = 800;

        const svg = d3.select("body")
        .append("svg")
        .attr("width", w)
        .attr("height", h)
        .style("margin-left",100);

        //CHARTBAR
        /*svg.selectAll("rect")
        .data(data)
        .enter()
        .append("rect")
        .attr("x", (d, i) => i * (w/data.length + data.length))
        .attr("y", (d, i) => h - 2 * d.value)
        .attr("width", (d,i) => w / data.length)
        .attr("height", (d, i) => d.value * 2)
        .attr("fill", "green");

        svg.selectAll("text")
        .data(data)
        .enter()
        .append("text")
        .text((d) => d.key)
        .attr("x", (d, i) => i * (w/data.length) + 20)
        .attr("y", (d, i) => h - (2 * d.value) - 3)
        */

        //CIRCULOS
        
        svg.selectAll("g")
        .data(data)
        .enter()
        .append("g")
        .append("circle")
        .attr("cx", (d,i) => d.value + w/10)
        .attr("cy", (d, i) => h/2)
        .attr("r",(d, i) => d.value/data.length * 10)
        .style("fill",function() {
            return "hsl(" + Math.random() * 360 + ",100%,50%)";
            })
    
        svg.selectAll("text")
        .data(data)
        .enter()
        .append("text")
        .text((d) => d.key)
        .attr("x", (d, i) => d.value + w/10)
        .attr("y", (d, i) => h/2)
        
    }

}
 
export default RandataBarChart;